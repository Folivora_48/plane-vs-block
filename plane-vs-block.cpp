#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

float obstacle_speed =  0.3;
float plane_speed = 0.6;
bool dead = false;

int main()
{
	sf::Music love_song;
	love_song.openFromFile("Sounds/what_is_love.flac");
	love_song.play();
	love_song.setLoop(true);
	
	sf::Font font;
	if(!font.loadFromFile("Fonts/DejaVuSans.ttf"))
	{
		std::cout << "Can't load font" << std::endl;
	}

	
	sf::Text level_text;
	level_text.setFont(font);
	level_text.setCharacterSize(24);
	level_text.setColor(sf::Color::Red);
	level_text.setString("Level: 1");
	
	sf::Text lives_text;
	lives_text.setFont(font);
	lives_text.setCharacterSize(24);
	lives_text.setColor(sf::Color::Red);
	lives_text.setString("lives: 1");
	lives_text.setPosition(0, 24);
	
	sf::Text fail;
	fail.setFont(font);
	fail.setCharacterSize(320);
	fail.setColor(sf::Color::Red);
	fail.setString("You failed!");
	fail.setPosition(1920/2 - 900, 1080/2 + 20);
	fail.setRotation(-20);
	
	sf::Text fps_text;
	fps_text.setFont(font);
	fps_text.setCharacterSize(24);
	fps_text.setColor(sf::Color::Red);
	fps_text.setPosition(0, 48);

	
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "Simple-Platformer");

	window.setFramerateLimit(400);
	window.setVerticalSyncEnabled(false);


	sf::View view;
	view.reset(sf::FloatRect(0, 0, 1920, 1080));
	view.setViewport(sf::FloatRect(0, 0, 1.f, 1.f));
	window.setView(view);

	// Textures
	sf::Texture plane_texture;
	if (!plane_texture.loadFromFile("Textures/plane_texture.png"))
	{
		std::cout << "ERROR: Can't find plane_texture.png" << std::endl;
	}

	// Sprites
	sf::Sprite plane_body;
	plane_body.setTexture(plane_texture);
	plane_body.setScale(34, 114);
	plane_body.setColor(sf::Color(0, 255, 0));
	plane_body.setPosition(1920/2, 1080/2);
	
	sf::Sprite plane_main_wing;
	plane_main_wing.setTexture(plane_texture);
	plane_main_wing.setScale(94, 20);
	plane_main_wing.setColor(sf::Color(0, 255, 0));
	plane_main_wing.setPosition(1920/2 - 63 / 2, 1080/2 + 24);
	
	sf::Sprite plane_tail_wing;
	plane_tail_wing.setTexture(plane_texture);
	plane_tail_wing.setScale(72, 16);
	plane_tail_wing.setColor(sf::Color(0, 255, 0));
	plane_tail_wing.setPosition(1920/2 - 40/ 2, 1080/2 + 80);
	
	
	/* obstacles */
	sf::Sprite obstacle1;
	obstacle1.setTexture(plane_texture);
	obstacle1.setScale(200,200);
	obstacle1.setColor(sf::Color(255, 50, 20));
	obstacle1.setPosition(0, 0);
	
	sf::Sprite obstacle2;
	obstacle2.setTexture(plane_texture);
	obstacle2.setScale(200,200);
	obstacle2.setColor(sf::Color(255, 50, 20));
	obstacle2.setPosition(350 - 6, 0);
	
	sf::Sprite obstacle3;
	obstacle3.setTexture(plane_texture);
	obstacle3.setScale(200,200);
	obstacle3.setColor(sf::Color(255, 50, 20));
	obstacle3.setPosition(700 - 12, 0);
	
	sf::Sprite obstacle4;
	obstacle4.setTexture(plane_texture);
	obstacle4.setScale(200,200);
	obstacle4.setColor(sf::Color(255, 50, 20));
	obstacle4.setPosition(1050 - 18, 0);
	
	sf::Sprite obstacle5;
	obstacle5.setTexture(plane_texture);
	obstacle5.setScale(200,200);
	obstacle5.setColor(sf::Color(255, 50, 20));
	obstacle5.setPosition(1400 - 24, 0);
	
	sf::Sprite obstacle6;
	obstacle6.setTexture(plane_texture);
	obstacle6.setScale(200,200);
	obstacle6.setColor(sf::Color(255, 50, 20));
	obstacle6.setPosition(1720, 0);
	
	sf::Sprite obstacle11;
	obstacle11.setTexture(plane_texture);
	obstacle11.setScale(200,200);
	obstacle11.setColor(sf::Color(255, 50, 20));
	obstacle11.setPosition(0, -540);
	
	sf::Sprite obstacle12;
	obstacle12.setTexture(plane_texture);
	obstacle12.setScale(200,200);
	obstacle12.setColor(sf::Color(255, 50, 20));
	obstacle12.setPosition(350 - 6, -540);
	
	sf::Sprite obstacle13;
	obstacle13.setTexture(plane_texture);
	obstacle13.setScale(200,200);
	obstacle13.setColor(sf::Color(255, 50, 20));
	obstacle13.setPosition(700 - 12, -540);
	
	sf::Sprite obstacle14;
	obstacle14.setTexture(plane_texture);
	obstacle14.setScale(200,200);
	obstacle14.setColor(sf::Color(255, 50, 20));
	obstacle14.setPosition(1050 - 18, -540);
	
	sf::Sprite obstacle15;
	obstacle15.setTexture(plane_texture);
	obstacle15.setScale(200,200);
	obstacle15.setColor(sf::Color(255, 50, 20));
	obstacle15.setPosition(1400 - 24, -540);
	
	sf::Sprite obstacle16;
	obstacle16.setTexture(plane_texture);
	obstacle16.setScale(200,200);
	obstacle16.setColor(sf::Color(255, 50, 20));
	obstacle16.setPosition(1720, -540);
	
	sf::Clock clock;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
			{
				window.close();
			}
        }
		sf::Time time = clock.getElapsedTime();

		obstacle1.move(0, obstacle_speed * time.asMilliseconds());
		obstacle2.move(0, obstacle_speed * time.asMilliseconds());
		obstacle3.move(0, obstacle_speed * time.asMilliseconds());
		obstacle4.move(0, obstacle_speed * time.asMilliseconds());
		obstacle5.move(0, obstacle_speed * time.asMilliseconds());
		obstacle6.move(0, obstacle_speed * time.asMilliseconds());
		obstacle11.move(0, obstacle_speed * time.asMilliseconds());
		obstacle12.move(0, obstacle_speed * time.asMilliseconds());
		obstacle13.move(0, obstacle_speed * time.asMilliseconds());
		obstacle14.move(0, obstacle_speed * time.asMilliseconds());
		obstacle15.move(0, obstacle_speed * time.asMilliseconds());
		obstacle16.move(0, obstacle_speed * time.asMilliseconds());
	
		//Keyboard
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
		{
			plane_speed = 0.3;
		}
		else
		{
			plane_speed = 0.6;
		}
		if (dead == true)
		{
			plane_speed = 0;
		}

		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			plane_body.move(-plane_speed * time.asMilliseconds(), 0);
			plane_main_wing.move(-plane_speed * time.asMilliseconds(), 0);
			plane_tail_wing.move(-plane_speed * time.asMilliseconds(), 0);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			plane_body.move(plane_speed * time.asMilliseconds(), 0);
			plane_main_wing.move(plane_speed * time.asMilliseconds(), 0);
			plane_tail_wing.move(plane_speed * time.asMilliseconds(), 0);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			plane_body.move(0, -plane_speed * time.asMilliseconds());
			plane_main_wing.move(0, -plane_speed * time.asMilliseconds());
			plane_tail_wing.move(0, -plane_speed * time.asMilliseconds());
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			plane_body.move(0, plane_speed * time.asMilliseconds());
			plane_main_wing.move(0, plane_speed * time.asMilliseconds());
			plane_tail_wing.move(0, plane_speed * time.asMilliseconds());
		}
		
		/* Colission */
		if (plane_body.getGlobalBounds().intersects(obstacle1.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle1.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle1.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle2.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle2.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle2.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle3.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle3.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle3.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle4.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle4.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle4.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle5.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle5.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle5.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_body.getGlobalBounds().intersects(obstacle6.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle6.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle6.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle11.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle11.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle11.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

			if (plane_body.getGlobalBounds().intersects(obstacle12.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle12.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle12.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

			if (plane_body.getGlobalBounds().intersects(obstacle13.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle13.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle13.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle14.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle14.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle14.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}

		if (plane_body.getGlobalBounds().intersects(obstacle15.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle15.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle15.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_body.getGlobalBounds().intersects(obstacle16.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_main_wing.getGlobalBounds().intersects(obstacle16.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
		if (plane_tail_wing.getGlobalBounds().intersects(obstacle16.getGlobalBounds()))
		{
			std::cout << "The sprite have collided" << std::endl;
			dead = true;
		}
	
		if (plane_body.getPosition().y < 0)
		{
			plane_body.setPosition(plane_body.getPosition().x, 0);
			plane_main_wing.setPosition(plane_main_wing.getPosition().x, 0 + 24);
			plane_tail_wing.setPosition(plane_tail_wing.getPosition().x, 80 );
		}
		if (plane_body.getPosition().y > 1080 - 114)
		{
			plane_body.setPosition(plane_body.getPosition().x, 1080 - 113 );
			plane_main_wing.setPosition(plane_main_wing.getPosition().x, 1080 - 113 + 24);
			plane_tail_wing.setPosition(plane_tail_wing.getPosition().x, 1080 -113 + 80 );
		}
		if (plane_main_wing.getPosition().x < 0)
		{
			plane_body.setPosition(30, plane_body.getPosition().y);
			plane_main_wing.setPosition(0, plane_main_wing.getPosition().y);
			plane_tail_wing.setPosition(11.5, plane_tail_wing.getPosition().y);
		}
		if (plane_main_wing.getPosition().x > 1920 - 94)
		{
			plane_body.setPosition(1920 - 94 + 30, plane_body.getPosition().y);
			plane_main_wing.setPosition(1920 - 94,  plane_main_wing.getPosition().y);
			plane_tail_wing.setPosition(1920 - 94 + 11.5, plane_tail_wing.getPosition().y);
		}
		/* fps per second */
		float current_fps = 1 / time.asSeconds();
		std::string current_fps_string = boost::lexical_cast<std::string>(current_fps);
		fps_text.setString("fps: " + current_fps_string);

		clock.restart();

		if (obstacle1.getPosition().y >= 1080)
		{
			obstacle1.setPosition(rand() % 72 + 0, - (rand() % 200) - 200);
		}
		if (obstacle2.getPosition().y >= 1080)
		{
			obstacle2.setPosition(rand() % 144 + 272, - (rand() % 200) - 200);
		}
		if (obstacle3.getPosition().y >= 1080)
		{
			obstacle3.setPosition(rand() % 144 + 616, - (rand() % 200) - 200);
		}
		if (obstacle4.getPosition().y >= 1080)
		{
			obstacle4.setPosition(rand() % 144 + 960, - (rand() % 200) - 200);
		}
		if (obstacle5.getPosition().y >= 1080)
		{
			obstacle5.setPosition(rand() % 144 + 1304, - (rand() % 200) - 200);
		}
		if (obstacle6.getPosition().y >= 1080)
		{
			obstacle6.setPosition(rand() % 72 + 1648, - (rand() % 200) - 200);
		}
		clock.restart();

		if (obstacle11.getPosition().y >= 1080)
		{
			obstacle11.setPosition(rand() % 72 + 0, obstacle1.getPosition().y -840 + rand() % 400);
		}
		if (obstacle12.getPosition().y >= 1080)
		{
			obstacle12.setPosition(rand() % 144 + 272, obstacle2.getPosition().y -840 + rand() % 400);
		}
		if (obstacle13.getPosition().y >= 1080)
		{
			obstacle13.setPosition(rand() % 144 + 616, obstacle3.getPosition().y -840 + rand() % 400);
		}
		if (obstacle14.getPosition().y >= 1080)
		{
			obstacle14.setPosition(rand() % 144 + 960, obstacle4.getPosition().y -840 + rand() % 400);
		}
		if (obstacle15.getPosition().y >= 1080)
		{
			obstacle15.setPosition(rand() % 144 + 1304, obstacle5.getPosition().y -840 + rand() % 400);
		}
		if (obstacle16.getPosition().y >= 1080)
		{
			obstacle16.setPosition(rand() % 72 + 1648, obstacle6.getPosition().y -840 + rand() % 400);
		}
		
		
		window.clear();
		window.setView(view);

		window.draw(plane_body);
		window.draw(plane_main_wing);
		window.draw(plane_tail_wing);
		window.draw(obstacle1);
		window.draw(obstacle2);
		window.draw(obstacle3);
		window.draw(obstacle4);
		window.draw(obstacle5);
		window.draw(obstacle6);
		window.draw(obstacle11);
		window.draw(obstacle12);
		window.draw(obstacle13);
		window.draw(obstacle14);
		window.draw(obstacle15);
		window.draw(obstacle16);
		window.draw(level_text);
		window.draw(lives_text);
		window.draw(fps_text);
		if (dead)
		{
			window.draw(fail);
			plane_speed = 0;
			obstacle_speed = 0;
		}
		
		window.display();
	}
	return 0;
}
